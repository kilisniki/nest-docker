import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import "reflect-metadata";

import {createConnection} from "typeorm";
import {Cat} from "./cat/cat.entity";

// createConnection({
//     type: "mongodb",
//     host: "localhost",
//     port: 27017,
//     username: "",
//     password: "",
//     database: "test",
//     entities: [
//         Cat
//     ],
//     synchronize: true,
//     logging: false
// }).then(async connection => {
//   console.log("SUCCESSFULL CONNECT");

//   //EXAMPLE 1
//   let cat = new Cat();
//   cat.name = 'Vasya';
//   cat.description = "typeorm main.ts connect and save";
//   cat.filename = "no";
//   cat.isPublished = false;
//   cat.views = 0;

//   //EXAMPLE 2
//   let cat2 = connection.manager.create(Cat);
//   cat2.id = 110;
//   cat2.name = 'Vasya2';
//   cat2.description = "typeorm main.ts connect, create and save";
//   cat2.filename = "no2";
//   cat2.isPublished = false;
//   cat2.views = 1;
//   //await connection.manager.save(cat2);

//   //EXAMPLE 3
//   //const catRepository = connection.getRepository(Cat);
//   //await catRepository.save(cat2);


//   // await connection.manager.save(connection.manager.create({
//   //   name: "Timber",
//   //   email: "Saw",
//   //   password: "test"
//   // }));

  
//   //console.log("cat has been saved. cat id is", cat2._id);
  
//     // here you can start to work with your entities
// }).catch(error => console.log(error));


async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: console
  });
  await app.listen(3000);
}
bootstrap();
