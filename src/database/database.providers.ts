import { createConnection, getConnectionOptions } from 'typeorm';

export const databaseProviders = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => await createConnection(
      await getConnectionOptions()
    //   {
    //   type: 'mongodb',
    //   host: 'localhost',
    //   port: 27017,
    //   username: '',
    //   password: '',
    //   database: 'test',
    //   entities: [
    //       __dirname + '/../**/*.entity{.ts,.js}',
    //   ],
    //   synchronize: true,
    // }
    ),
  },
];