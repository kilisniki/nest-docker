import { PipeTransform, Injectable, ArgumentMetadata } from "@nestjs/common";

//Преобразует айдишник из string в number

@Injectable()
export class CatIdPipe implements PipeTransform<any, number> {

    transform(value: any, metadata: ArgumentMetadata): number {

        console.log('IDPipe value: ', value);       

        return parseInt(value,10);    
    }

}