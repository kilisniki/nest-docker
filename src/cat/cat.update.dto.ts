import { IsString, IsNotEmpty, IsNumber } from 'class-validator'
 
export class UpdateCatDto {

    @IsString()
    name: string;

    @IsNotEmpty()
    description: string;

    @IsNumber()
    views: number;

}