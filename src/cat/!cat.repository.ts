import { Repository, EntityRepository, getConnectionOptions } from "typeorm";
import { Cat } from "./cat.entity";
import "reflect-metadata";
import {createConnection} from "typeorm";


//@Injectable()
//@EntityRepository(Cat)
export class CatsRepository extends Repository<Cat> {

    public static async getRepo() : Promise<Repository<Cat>>{
        console.log();
        
        const connectionOptions = await getConnectionOptions();
        console.log('Connection options:', connectionOptions);
        
        const connection = await createConnection(
            connectionOptions
        )

        return connection.getRepository(Cat);
    }
}