import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";

import { Observable } from "rxjs";

//Если в body запроса указано поле imdog: true то запрещает доступ к роуту

@Injectable()
export class CatGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    console.log('CAT GUARD: ', request.body['imdog']);
    
    return  validateRequest(request);
  }
}

async function validateRequest(request: Request): Promise<boolean> {
    if(Boolean(request.body['imdog'])==true){
        return false;
    } else {
        return true;
    }
}