import { Catch, NotFoundException, ExceptionFilter, ArgumentsHost } from "@nestjs/common";

@Catch(NotFoundException)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: NotFoundException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const status = exception.getStatus();

    
    response
    .status(status)
    .json({
        statusCode: status+1,
        timestamp: new Date().toISOString(),
        path: request.url,
      });
  }
}
//Мы нигде в коде не указали использование этого фильтра, почему он работает???