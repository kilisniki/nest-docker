import { IsString, IsNotEmpty} from 'class-validator';

export class CreateAutonameCatDto {

    @IsNotEmpty()
    description: string;

}