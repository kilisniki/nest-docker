import { Injectable, Inject } from '@nestjs/common';
import { Cat } from './cat.entity';
import { CreateCatDto } from './dto/cat.create.dto';
import { UpdateCatDto } from './cat.update.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class CatsService {
  private readonly cats: Cat[] = [];
  constructor(
    @InjectRepository(Cat)
    private readonly catRepository: Repository<Cat>,
  ) {}


  //Create Cat
  async create(createCatDto: CreateCatDto): Promise<number> {
    const newcat = this.catRepository.create(createCatDto);
    console.log("NEW createCAT IN REPOSITORY: ", newcat);
    
    await this.catRepository.save(newcat);

    console.log("NEW CAT IN DB: ", newcat);

    return newcat.id;
  }

  //FIND CATS
  async findAll(): Promise<Cat[]> {
    return await this.catRepository.find();
    //return this.cats;
  }

  //FIND ONE CAT
  async findOne(id: number): Promise<Cat> {
    return await this.catRepository.findOne({id: id});
  }

  //UPDATE CAT
  async updateOne(id: number, updateCatDto: UpdateCatDto): Promise<Cat> {
    const cat = await this.findOne(id);
    cat.name = updateCatDto.name;
    cat.description = updateCatDto.description;
    cat.views = updateCatDto.views;

    this.catRepository.save(cat);
    return cat;
  } 
}