import { NotFoundException } from "@nestjs/common";

export class CatNotFoundException extends NotFoundException {
    constructor() {
        super('Cat not foundddd', 'MyMessageError');
      }
}