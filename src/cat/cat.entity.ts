import {Entity, Column, ObjectID, ObjectIdColumn} from "typeorm";

@Entity()
export class Cat {

    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    id: number;

    @Column({
        length: 100
    })
    name: string;

    @Column("text")
    description: string;

    @Column()
    filename: string;

    @Column()
    views: number;

    @Column()
    isPublished: boolean;
}