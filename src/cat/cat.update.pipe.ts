import { PipeTransform, Injectable, ArgumentMetadata } from "@nestjs/common";
import { UpdateCatDto } from "./cat.update.dto";

@Injectable()
export class CatUpdatePipe implements PipeTransform<any, number | UpdateCatDto> {

    transform(value: any, metadata: ArgumentMetadata): number | UpdateCatDto {

        console.log('UpdateCatPipe value: ', value);

        //console.log(metadata, metadata.data, metadata.metatype, metadata.type);        

        //Этот pipe используется в запросе апдейтВан, у него есть Param и Body
        //т.к. этот пайп проходят вразнобой, то надо в метаданных проверять что нам сейчас пришло
        if(metadata.data==='id'){
            console.log('id === id');
            
            return value;
        }
        

        const cat: UpdateCatDto = {
            //id: parseInt(value.id, 10),
            name: value.name,
            description: value.description,
            views: parseInt(value.views, 10)
        }

        console.log(cat, typeof cat.views);
        

        return cat;    
    }

}