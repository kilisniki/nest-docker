import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { ValidatorOptions, ValidationError } from 'class-validator';


//Не работает, разобраться
export class CatsValidatorOptions implements ValidatorOptions {
    skipMissingProperties: true;
    // exceptionFactory: (args: string[]) => {args; throw BadRequestException()}
}