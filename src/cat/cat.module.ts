import { Module } from '@nestjs/common';
import { CatsController } from './cat.controller';
import { CatsService } from './cat.service';
import { CatUpdatePipe } from './cat.update.pipe';
import { CatIdPipe } from './cat.id.pipe copy';
import { CatGuard } from './cat.guard';
//import { catProviders } from './!cat.providers';
import { DatabaseModule } from 'src/database/database.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cat } from './cat.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Cat])
  ],
  controllers: [CatsController],
  providers: [
      CatsService,
      CatIdPipe,
      CatUpdatePipe,
      CatGuard,
    //  ...catProviders,
    CatsService,
    ],
    exports: [TypeOrmModule]
})
export class CatsModule {}
