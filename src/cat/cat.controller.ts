import { Controller, Get, Post, Body, Inject, UsePipes, ValidationPipe, Param, UseGuards } from '@nestjs/common';
import { Cat } from './cat.entity'; 
import { CreateCatDto } from './dto/cat.create.dto';
import { CreateAutonameCatDto } from './dto/cat.create.autoname.dto';
import { CatsService } from './cat.service';
import { CatByIdPipe } from './cat.by.id.pipe';
import { UpdateCatDto } from './cat.update.dto';
import { CatUpdatePipe } from './cat.update.pipe';
import { CatIdPipe } from './cat.id.pipe copy';
import { CatGuard } from './cat.guard';
import { CatDecorator } from './cat.decorator'

@Controller('cats')
export class CatsController {

  constructor(private readonly catsService: CatsService){}

  @Post()
  @UsePipes(ValidationPipe)
  async create(@Body() createCatDto: CreateCatDto): Promise<number> { //@Body не парсит входящие значения, а только подгоняет в объект req поля, которые содержатся в CreateCatDto
    const id = await this.catsService.create(createCatDto); // И обрезает лишние поля
    return id;
  }

  //try CustomDecorator
  @Post('autoname')
  async createWithMyDecorator(@CatDecorator(['Nensy', 'Tom', 'Alibeck']) catname: string, @Body() createAutonameCatDto: CreateAutonameCatDto): Promise<number> {
    const createCatDto: CreateCatDto = {
      description: createAutonameCatDto.description,
      name: catname
    }
    const id = await this.catsService.create(createCatDto); // И обрезает лишние поля
    return id;
  }


  @Post('guard')
  @UseGuards(CatGuard)
  async findAll(): Promise<Cat[]> {
    return await this.catsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id', CatByIdPipe) cat: Promise<Cat>): Promise<Cat> {
  const catobj = await cat;
    console.log(catobj);
    return catobj;
  }


  //Довольно сложная логика:
  //функция updateOne запрашивает одновременно @Param и @Body
  // Сначала аргументы проходят через CatUpdatePipe, потом через свой собственный (для Param это CatIdPipe)
  // при чем сначала проходит проверки Body а потом Param
  // 
  @Post(':id')
  @UsePipes(CatUpdatePipe)
  async updateOne(@Param('id', CatIdPipe) id: number, @Body() param: UpdateCatDto): Promise<Cat> {
    //FUCKING CASE:
    //updateOne(@Param() id: number, @Body() params: any): Cat {
    //POST Controller params:  { id: NaN, name: 'Viktor', description: 'not a dog', views: 100500 } 
    // id:  { id: 0, name: undefined, description: undefined, views: NaN }
    //updateCatDto.views = Number.parseInt(updateCatDto.views.toString());
    console.log('POST Controller params: ', id, ' id: ',param);
    
    return this.catsService.updateOne(id, param);
  }
}