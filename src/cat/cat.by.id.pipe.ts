import { Cat } from "./cat.entity";
import { PipeTransform, Injectable, ArgumentMetadata } from "@nestjs/common";
import { CatsService } from "./cat.service";
import { CatNotFoundException } from "./cat.notfound.exception";

@Injectable()
export class CatByIdPipe implements PipeTransform<string, Promise<Cat>> {

    constructor(private readonly catService: CatsService){}

    async transform(value: string, metadata: ArgumentMetadata): Promise<Cat> {

        const cat = await this.catService.findOne(Number.parseInt(value));

        if(cat!=null){
            return cat;
        } else {
            throw new CatNotFoundException();
        }        
    }

}