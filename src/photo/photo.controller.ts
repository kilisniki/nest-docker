import { Controller, Get, Post, UseInterceptors } from '@nestjs/common';
import { PhotoService } from './photo.service';
import { Photo } from './photo.entity';
import { CreatePhotoDto } from './photo.create.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';

@Controller()
export class PhotoController {
  constructor(private readonly appService: PhotoService) {}

  @Get()
  async getHello(): Promise<Photo> {
    return await this.appService.findOne(0);
  }

  @Post()
  @UseInterceptors(FileFieldsInterceptor([
    { name: 'photo', maxCount: 1 },
  ]))
  async newPhoto(): Promise<number> {
      return await this.appService.create(new CreatePhotoDto());
  }
}
