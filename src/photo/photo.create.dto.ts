import { IsString, IsNotEmpty} from 'class-validator';
import { PhotoState } from './photo.entity';

export class CreatePhotoDto {

 

    @IsString()
    @IsNotEmpty()
    driver_id: number;

    @IsString()
    @IsNotEmpty()
    filename: string;

    @IsString()
    @IsNotEmpty()
    original_filename: string;

    @IsString()
    @IsNotEmpty()
    directory: string;

    @IsNotEmpty()
    state: any;
}