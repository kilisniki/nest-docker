import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Photo } from './photo.entity';
import { CreatePhotoDto } from './photo.create.dto';


@Injectable()
export class PhotoService {
  private readonly cats: Photo[] = [];
  constructor(
    @InjectRepository(Photo)
    private readonly photoRepository: Repository<Photo>,
  ) {}


  //Create Photo
  async create(createPhotoDto: CreatePhotoDto): Promise<number> {
    const newcat = this.photoRepository.create(createPhotoDto);
    console.log("NEW createCAT IN REPOSITORY: ", newcat);
    
    await this.photoRepository.save(newcat);

    console.log("NEW CAT IN DB: ", newcat);

    return newcat.driver_id;
  }

//   //FIND CATS
//   async findAll(): Promise<Cat[]> {
//     return await this.catRepository.find();
//     //return this.cats;
//   }

  //FIND ONE CAT
  async findOne(id: number): Promise<Photo> {
    return await this.photoRepository.findOne({driver_id: id});
  }

//   //UPDATE CAT
//   async updateOne(id: number, updateCatDto: UpdateCatDto): Promise<Cat> {
//     const cat = await this.findOne(id);
//     cat.name = updateCatDto.name;
//     cat.description = updateCatDto.description;
//     cat.views = updateCatDto.views;

//     this.catRepository.save(cat);
//     return cat;
//   } 
}