import {Entity, Column, ObjectID, ObjectIdColumn} from "typeorm";

export enum PhotoState {
    NEW,
    ARCHIVE
}

@Entity()
export class Photo {

    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    driver_id: number;

    @Column()
    filename: string;

    @Column()
    original_filename: string;

    @Column()
    directory: string;

    @Column({
        type: "enum",
        enum: PhotoState,
        //default: PhotoState.NEW
    })
    state: Photo;

}