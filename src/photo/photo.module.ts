import { Module } from '@nestjs/common';

//import { catProviders } from './!cat.providers';
import { DatabaseModule } from 'src/database/database.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Photo } from './photo.entity';
import { PhotoController } from './photo.controller';
import { PhotoService } from './photo.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Photo])
  ],
  controllers: [PhotoController],
  providers: [
      PhotoService,
    //  ...catProviders,
    ],
    exports: [TypeOrmModule]
})
export class PhotoModule {}
