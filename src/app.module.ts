import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cat/cat.module';
import { APP_FILTER } from '@nestjs/core';
import { CatNotFoundException } from './cat/cat.notfound.exception';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cat } from './cat/cat.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'db_mongo', //!!! в контейнере не localhost!!!
      port: 27017,
      username: '',
      password: '',
      database: 'test',
      entities: [Cat], //!!!Проверяй что здесь указаны сущности!!!
      synchronize: true,
    }),
    CatsModule
  ],
  controllers: [AppController],
  providers: [AppService,
    {
        provide: APP_FILTER,
        useClass: CatNotFoundException,
    },
    // {
    //   provide: APP_FILTER,
    //   useClass: AllExceptionFilter,
    // }
  ],
})
export class AppModule {}
