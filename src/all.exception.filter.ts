import { Catch, NotFoundException, ExceptionFilter, HttpException, ArgumentsHost } from "@nestjs/common";

//Перекрывает все остальные фильтры, ВТФ???

@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    
    
    console.log(`Wow! It's Global log: \n
      request: ${request.url}
      time: ${new Date()}
      ip: ${request.headers['x-forwarded-for'] || request.connection.remoteAddress}`);
    
    
    response
    .json({
        statusCode: 100500,
        timestamp: new Date().toISOString(),
        path: request.url,
        message: "UNKNOWN SERVER EXCEPTION OR ERROR?"
      });
  }
}
//Мы нигде в коде не указали использование этого фильтра, почему он работает???